export function withDraw(number) {
  return {
    type: "WITHDRAW",
    amount: number
  }
}