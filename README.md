# Understanding Redux 1 #
**This is sample projects, which were made by me, of Understanding Redux 1 ebooks.**

![Understanding Redux 1](https://cdn-images-1.medium.com/max/2000/1*8cpJBanzu5koQqzkBirvsQ.png)

This includes 4 samples projects: 00-hello-wolrd, 01-user-card, 02-bank-app, 03-skypey and a document.

I hope it will give you an overview of Redux.
Thanks for watching and see you again in next books!