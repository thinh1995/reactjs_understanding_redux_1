import React, { Component } from 'react'

export default class HelloWorld extends Component {
  render() {
    return (
      <div>
        Hello World, {this.props.tech}
      </div>
    )
  }
}
